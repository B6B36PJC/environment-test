cmake_minimum_required(VERSION 3.5)

project(pjc-check LANGUAGES CXX VERSION 0.0.1)

# Force C++11, also disable GNU extensions
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

add_executable(to_string-check to_string-check.cpp)
add_executable(cpp14-check cpp14-check.cpp)


set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)
add_executable(threading-check threading-check.cpp)
target_link_libraries(threading-check PRIVATE Threads::Threads)

enable_testing()

add_test(NAME string-check COMMAND to_string-check)
set_tests_properties(
    string-check
  PROPERTIES
    PASS_REGULAR_EXPRESSION "123"
)

add_test(NAME c++14-check COMMAND cpp14-check)
set_tests_properties(
    c++14-check
  PROPERTIES
    PASS_REGULAR_EXPRESSION "11"
)

add_test(NAME thread-check COMMAND threading-check)
set_tests_properties(
    thread-check
  PROPERTIES
    PASS_REGULAR_EXPRESSION "39204"
)
