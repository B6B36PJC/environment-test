#include <iostream>

int test(int x) {
    if (x > 0) {
        return 1;
    }
    x -= 1'000'000;
    if (x > 0) {
            return 2;
    }
    return 1;
}

int main(){
    int a = 1'000;
    int b = 1'111'222;
    std::cout << a + b << '\n';

    int c = [](auto lhs, auto rhs){ return lhs + rhs; }(10, 1);
    std::cout << c << '\n';
}
